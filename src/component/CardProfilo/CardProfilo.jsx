import profiloImg from "../../assets/profilo.jpg"
import { useContext, useId} from "react";
import { AuthContext } from "../../contexts/AuthContext";

export default function CardProfilo() {

    const {currentUser} = useContext(AuthContext)
    const randId = useId()

    return (
        <div className="card" style={{ width: "18rem" }}>
          <img src={profiloImg} className="card-img-top" alt="..." />
          <div className="card-body">
            <h5 className="card-title">
                I miei dati: <br/>
              Nome: {currentUser.nome} <br/>
              Cognome: {currentUser.cognome}
            </h5>
            <h5 className="card-title">E-mail: {currentUser.email}</h5>
            {currentUser?.ruoli?.map((ruolo) => (
              <h5 key={randId} className="card-title">{ruolo.tipologia}</h5>
            ))}
           
          </div>

          
        </div>
      );
}