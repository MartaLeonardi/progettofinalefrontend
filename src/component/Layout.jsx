import { useOutlet } from "react-router-dom";
import Navbar from "./Navbar/Navbar";


export default function Layout() {
  const outlet = useOutlet();

  return (
 
    <div>
      <Navbar></Navbar>
        {outlet}

    </div>
  );
}
