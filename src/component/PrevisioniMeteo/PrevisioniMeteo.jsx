
import {useState, useEffect} from "react";
import { previsioneMeteo } from "../../services/RESTService";

export default function PrevisioniMeteo (){

    const [infoMeteo, setInfoMeteo] = useState ({})
    const { base, clouds, cod, coord, dt, id, main, name, sys, timezone, visibility, weather, wind } = infoMeteo;


    const [nomeCitta, setNomeCitta] = useState([])


    //gestione dell'evento per l'inserimento dei dati all'interno dei campi
    const handleChange = (e) => {
        const {name, value} = e.target
        setNomeCitta(value)
    }

    //gestione evento per l'invio dei dati
    const handleSubmit=(e) => {
    e.preventDefault();
    console.log(nomeCitta)
    const response = previsioneMeteo(nomeCitta);
    console.log(response)

    setInfoMeteo(response)
    console.log(infoMeteo)

}

  
    return(
        <>
        <form onSubmit={handleSubmit}>
        <div className="card mt-5 shadow" style={{ width: "18rem" }} >
            <input
            name="citta"
            type="text"
            id="citta"
            placeholder="Città"
            onChange={handleChange} 
            value={nomeCitta}
            required
            />

        <button type="submit" className="btn btn-outline-primary">
            Effettua previsione meteo
        </button>
        </div>
        </form>
        
        </>
    )
}