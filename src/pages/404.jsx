import { useEffect } from "react";
import { Link } from "react-router-dom"


export default function NotFound () {
  useEffect(() => {
    document.title = "Page Not Found";
  }, []);

  return (
  <>
  <h1>PAGINA NON TROVATA!</h1>
  <Link to="/">Torna alla home</Link>
  </>);
}
