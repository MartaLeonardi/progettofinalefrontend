import { useContext, useEffect, useState } from "react";
import { getUtenti } from "../services/RESTService";
import { AuthContext } from "../contexts/AuthContext";
import { useNavigate } from "react-router-dom";

export default function Utenti() {
  const { currentUser } = useContext(AuthContext);

  const [allUsers, setAllUsers] = useState([]);

  const [tableVisibility, setTableVisibility] = useState(true);

  const navigate = useNavigate();

  const fetchData = async () => {
    const allUsers = await getUtenti();
    setAllUsers(allUsers);
  };

  useEffect(() => {
    //se l'utente loggato non è Admin vede la pagina 404 not found
    if (!currentUser.ruoli.includes("Admin")) {
        navigate("/notFound")
      } else if (currentUser.nome == "") {
      navigate("/login", { replace: true });
    } else {
      document.title = "Lista utenti";
      fetchData();
    }
  }, []);


  return (
    <>
      <div className="container-fluid">
        <div className="row">
          <div className="col-12">
            {tableVisibility ? (
              <table className="table">
                <thead>
                  <tr>
                    <th scope="col" className="text-center">
                      ID
                    </th>
                    <th scope="col" className="text-center">
                      Nome
                    </th>
                    <th scope="col" className="text-center">
                      Cognome
                    </th>
                    <th scope="col" className="text-center">
                      Email
                    </th>
                    <th scope="col" className="text-center">
                      Ruolo
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {allUsers?.map((user) => (
                    <tr key={user.idUtente}>
                      <th scope="row" className="text-center">
                        {user.idUtente}
                      </th>
                      <td className="text-center">{user.nome}</td>
                      <td className="text-center">{user.cognome}</td>
                      <td className="text-center">{user.email}</td>
                      <td className="text-center">
                        <div className="d-flex flex-column">
                          {user.ruoli.length == 0 ? (
                            <span key={Math.random()}>Utente</span>
                          ) : (
                            user.ruoli.map((ruolo) => (
                              <span key={Math.random()}>{ruolo.tipologia}</span>
                            ))
                          )}
                        </div>
                      </td>
                     
                    </tr>
                  ))}
                </tbody>
              </table>
            ) : (
              <></>
            )}

           
          </div>
        </div>
      </div>
    </>
  );
}
