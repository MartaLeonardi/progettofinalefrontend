
import { useEffect } from "react";
import RegistrazioneForm from "../component/RegistrazioneForm/RegistrazioneForm.jsx";

export default function Register() {
  useEffect(() => {
    document.title = "Registrati";
  }, []);

  return (
    <>
      <RegistrazioneForm></RegistrazioneForm>
    </>
  );
}
