import { AuthContext } from "../contexts/AuthContext";
import { useContext, useEffect } from "react";
import LoginForm from "../component/LoginForm/LoginForm";

export default function Login() {
    const {setCurrentUser} = useContext(AuthContext)
  
    useEffect(() => {
      document.title = "Accedi";
    }, []);
  
    return (
      <>
        
        <LoginForm setCurrentUser={setCurrentUser}></LoginForm>
      </>
    );
  }
  