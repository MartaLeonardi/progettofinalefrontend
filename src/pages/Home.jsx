import { useEffect } from "react";
import previsioniMeteo from "../assets/previsioniMeteo.jpg"

export default function Home() {
  useEffect(() => {
    document.title = "Home";
  }, []);

  return (
  <>

  <div style={{textAlign: "center", alignItems: "center"}}>
    <h1>Benvenuto!</h1>
    <p>Effettua l'accesso per effettuare una previsione meteo!</p>
    <img src={previsioniMeteo} className="rounded mx-auto d-block img-thumbnail" alt="immagine HomePage" style={{maxWidth:"50%"}}/>

  </div>
    
  </>);
}
