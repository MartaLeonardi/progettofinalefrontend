
import { useEffect } from "react";
import CardProfilo from "../component/CardProfilo/CardProfilo";
import PrevisioniMeteo from "../component/PrevisioniMeteo/PrevisioniMeteo";


export default function Profilo() {
  useEffect(() => {
    document.title = "Profilo utente";
  }, []);

  return (
    <>
      <CardProfilo />

      <div>
        <PrevisioniMeteo/>
      </div>
    </>
  );
}
