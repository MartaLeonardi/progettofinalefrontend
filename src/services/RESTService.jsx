import { URLs } from './config/rest-service-config'
import Cookies from "js-cookie";
import { jwtDecode } from "jwt-decode";
import { jwtExpirations } from './config/rest-service-config';
import { cookieTypes } from './config/rest-service-config';

//registrazione
export async function registrazioneUtente(formData){
  const jsonData = JSON.stringify(formData)
  const response = await fetch(URLs.registerUrl, {
    mode: "cors",
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: jsonData
  })
  return response.status
}

//login
export async function loginUtente(formData){
  const jsonData = JSON.stringify(formData)
  const response = await fetch(URLs.loginUrl, {
    mode: "cors",
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: jsonData
  })
  return response
}

//recupero tutti gli utenti -- FUNZIONE SOLO PER ADMIN
export async function getUtenti(){
  try{
    const response = await fetch(URLs.getAllUsersUrl, {
      mode: "cors",
      method: "GET",
    headers: {
      "Authorization": getBearerToken(cookieTypes.jwt)
    }
    })
    const allUsersData = await response.json()
    return allUsersData

  }catch(error){
    return null
  }
}


//METODO GET DELLA PREVISIONE METEO DELL'API openweather
export async function previsioneMeteo(citta) {

  const response = await fetch(`https://api.openweathermap.org/data/2.5/weather?q=${citta}&appid=22d9136c8b5617662320dd4f3eafd761`, {
    mode: "cors",
    method: "GET",
  })
if(response.status == 200){
  return await response.json();
}else{
  return response.status
}

}

//cancello il cookie
export function deleteCookie(type){
    Cookies.remove(type)
  }
  
  //salvo il jwt nei cookies
  export function setLoginCookie(jwtToken) {
    const jwtString = JSON.stringify(jwtToken.token)
    Cookies.set(cookieTypes.jwt, jwtString, {expires: jwtExpirations.oneMonth})
    return jwtDecode(jwtString)
  }
  
  //controllo che esista il jwt e, se esiste, lo decodifico per leggere i dati dell'utente
  export function checkLoginCookie(type){
    const loginCookie = Cookies.get(type)
    if(loginCookie != undefined){
      return jwtDecode(loginCookie)
    } else {
      return null
    }
  }
  
  //controllo che esista il jwt e, se esiste, lo estraggo concatenato con Bearer
  export function getBearerToken(type){
    const loginCookie = Cookies.get(type)
    const shortToken = loginCookie.substring(1, loginCookie.length - 1)
    if(shortToken != undefined){
      return "Bearer " + shortToken
    } else {
      return null
    }
  }
  