export const URLs = {
    registerUrl: 'http://localhost:8080/apiMeteo/utente/registrazione',
    loginUrl: 'http://localhost:8080/apiMeteo/utente/login',
    getAllUsersUrl: 'http://localhost:8080/apiMeteo/utente/getUtenti',
  }

  
  export const jwtExpirations = {
    oneYear: new Date().getFullYear(),
    oneMonth: 31,
    oneWeek: 7,
    oneDay: 1
  }
  
  export const courseCategories = {
    FrontEnd: 1,
    BackEnd: 2,
    FullStack: 3,
    CyberSecurity: 4
  }
  
  export const userRoles = {
    Admin: 1,
    Teacher: 2
  }
  
  export const cookieTypes = {
    jwt: "JWT"
  }