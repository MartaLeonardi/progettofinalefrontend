import React from 'react'
import ReactDOM from 'react-dom/client'
import { createBrowserRouter } from "react-router-dom";
import { RouterProvider } from "react-router-dom";
import "../node_modules/bootstrap/dist/js/bootstrap.bundle.js";
import "../node_modules/bootstrap/dist/css/bootstrap.css";
import "../node_modules/bootstrap-icons/font/bootstrap-icons.css";
import AuthContextProvider from "./contexts/AuthContextProvider.jsx";
import Layout from "../src/component/Layout.jsx";
import './index.css'
import Home from "./pages/Home.jsx";
import Login from "./pages/Login.jsx";
import Registrazione from "./pages/Registrazione.jsx";
import Profilo from './pages/Profilo.jsx';
import Utenti from './pages/Utenti.jsx';
import NotFound from './pages/404.jsx';

const router = createBrowserRouter(
  [
    {
      element: <AuthContextProvider><Layout /></AuthContextProvider>,
      children: [
        {
          path: "/",
          element: <Home />
        },
        {
          path: "login",
          element: <Login />,
        },
        {
          path: "registrazione",
          element: <Registrazione />,
        },
        {
          path: "utente/profilo",
          element: <Profilo/>
        },
        {
          path: "utenti",
          element : <Utenti/>,
        },
        {
          path: "*",
          element: <NotFound />,
        }
        
      ],
    },
  ],
);

ReactDOM.createRoot(document.getElementById('root')).render(
  <RouterProvider router={router} />
)
